#!/usr/bin/env python3

import unittest
from app import merge_pdf_files

class TestImageToPDF(unittest.TestCase):
    def test_merge_pdf_files(self):
        outfile = 'sample-files/merged.pdf'
        files = ["sample-files/back_flip.pdf", "sample-files/back_flip_copy.pdf"]
        test_case = merge_pdf_files(outfile, files)
        self.assertEqual(test_case, outfile)
       


if __name__ == "__main__":
    unittest.main()