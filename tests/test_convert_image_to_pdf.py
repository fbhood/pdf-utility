#!/usr/bin/env python3

import unittest
from app import convert_image_to_pdf

class TestImageToPDF(unittest.TestCase):
    def test_returns_a_pdf(self):
        img_path = "sample-files/back_flip.jpg"
        test_case = convert_image_to_pdf(img_path)
        expected = "sample-files/back_flip.pdf"
        self.assertTrue(test_case, expected)


if __name__ == "__main__":
    unittest.main()