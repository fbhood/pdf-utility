#!/usr/bin/env python3
import unittest
from app import compress_pdf
import sys
import os


class TestCompressPDF(unittest.TestCase):
    def test_basic(self):
        outfile = "sample-files/cert_compr.pdf"
        file = "sample-files/coursera_cert.pdf"
        test_case = compress_pdf(outfile, file)
        expected = outfile
        self.assertEqual(test_case, expected)


    def test_file_was_compressed(self):
        outfile = "sample-files/cert_compr.pdf"
        file = "sample-files/coursera_cert.pdf"
        final_file = compress_pdf(outfile, file)
        testCase = os.path.getsize(final_file) < os.path.getsize(file)
        expect = True
        self.assertTrue(testCase, expect)
        

    def test_output_file_not_empty(self):
        outfile = ""
        file = "sample-files/coursera_cert.pdf"
        test_case = compress_pdf(outfile, file)
        expected = "sample-files/coursera_cert_compressed.pdf"
        self.assertEqual(test_case, expected)
    

        


if __name__ == "__main__":
    unittest.main()
