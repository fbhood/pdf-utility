#!/usr/bin/env python3
import unittest
from app import get_images

class TestGetImages(unittest.TestCase):
    def test_basic(self):
        img_path = "sample-files/"
        test_case = get_images(img_path)
        expected = list
        self.assertIsInstance(test_case, expected)

    def test_only_images_are_returned(self):
        img_path = "sample-files/"
        test_case = get_images(img_path)
        expected = ["back_flip_copy.jpg", "back_flip.jpg"]
        self.assertListEqual(test_case, expected)


if __name__ == "__main__":
    unittest.main()